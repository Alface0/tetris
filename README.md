# Tetris game with PyGame

Implementation of a tetris clone using the pygame engine

## Dependencies

* Install pygame
```sh
pip install pygame
```

## Run

* Run with a graphical interface
```sh
python main.py
```

* Run in command line mode using ncurses (Not Finished)
```sh
python main.py CONSOLE
```

## Playing preview

To see a preview of the game being played check the [video](https://www.youtube.com/watch?v=zmVWVMobMHs).

## Screenshots

![Start Menu](/screenshots/menu.png)
![Credits](/screenshots/credits.png)
![Settings](/screenshots/settings.png)
![Select Name](/screenshots/name_selection.png)
![In Game](/screenshots/ingame1.png)
![In Game](/screenshots/ingame2.png)
![In Game](/screenshots/ingame3.png)
![In Game](/screenshots/ingame4.png)
![In Game](/screenshots/ingame5.png)
![Score](/screenshots/score.png)

## Sources

* Pygame [link](https://www.pygame.org/)
* Tetris original music [link](https://archive.org/details/TetrisThemeMusic)
* NCurses [link](https://www.gnu.org/software/ncurses/)