# TODO

1. ~~Add sounds to the movements between the menu options~~
2. ~~Add sound when enter is pressed in a menu option~~
3. ~~Add blinking cursor in the name writing state~~
4. ~~Add sound when the game is lost~~ **All sounds looked stupid when played in the tetris game so NOT IMPLEMENTED!**
5. ~~Change the background color from a relaxing colour in the initial levels to a warning color in the more hard levels in order to make the player fell more endagered as long as the game progresses and make it get more survival pleasure~~
6. ~~Speed up a little bit the music of the game in the more andvanced levels to make the player feel more the speed of the game and obtain a notion of progress and skill evolution~~
7. ~~Add animation of a line disapearing when a line is complete~~
8. ~~Change the background colors ingame to a less intrusive pattern, like a gradient~~
9. Add some glow in the blocks
10. Fix the bug that sometimes a block overrides another block positions if rotated
11. Fix the bug that if there is no space to rotate sometimes the game crash
12. Create game icon
13. Use the pygame or other tool sound in order to be cross-platform
14. Deploy for Linux, Windows and Mac