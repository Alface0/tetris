import random
from constants import *
import copy

blocks = {
	"T": [
		[0,0,0,0],
		[0,1,1,1],
		[0,0,1,0],
		[0,0,0,0]
	],
	"L": [
		[0,0,0,0],
		[0,2,0,0],
		[0,2,0,0],
		[0,2,2,0]
	],
	"O" : [
		[0,0,0,0],
		[0,3,3,0],
		[0,3,3,0],
		[0,0,0,0]
	],
	"I": [
		[0,4,0,0],
		[0,4,0,0],
		[0,4,0,0],
		[0,4,0,0]
	],
	"J": [
		[0,0,0,0],
		[0,0,5,0],
		[0,0,5,0],
		[0,5,5,0]
	],
}

class Block(object):
	def __init__(self, block_type, position):
		self.x = position
		self.y = 0
		self.block_type = block_type
		self.block = copy.deepcopy(blocks[block_type])
		self.block_positions = []
		self.detect_block_positions()

	def print_block(self):
		for x in range(Constants.BLOCK_HEIGHT):
			print(self.next_block[x])

	def detect_block_positions(self):
		self.block_positions = []

		for row in range(Constants.BLOCK_HEIGHT):
			for col in range(Constants.BLOCK_WIDTH):
				if self.block[row][col] != 0:
					self.block_positions.append((row, col))

	def rotate_block(self):
		# Rotate 90 degrees
		new_block = [
			[self.block[3][0], self.block[2][0], self.block[1][0], self.block[0][0]],
			[self.block[3][1], self.block[2][1], self.block[1][1], self.block[0][1]],
			[self.block[3][2], self.block[2][2], self.block[1][2], self.block[0][2]],
			[self.block[3][3], self.block[2][3], self.block[1][3], self.block[0][3]]
		]

		self.block = new_block
		self.detect_block_positions()

class BlockBuilder(object):
	def __init__(self):
		self.block_types = list(blocks.keys())
		self.current_block = None
		
		# The block types group will be used to make the probabilities a block appearing change if it appears to much times
		self.last_blocks_types = []
		self.block_types_group = []
		for block_type in self.block_types:
			self.block_types_group.extend([block_type] * Constants.MAX_BLOCKS_OF_SAME_TYPE)

		random_block_type = self.block_types_group[random.randrange(len(self.block_types_group))]
		self.last_blocks_types.append(random_block_type)

		random_position = random.randrange(Constants.MAP_WIDTH - Constants.BLOCK_WIDTH)
		self.next_block = Block(block_type=random_block_type, position=random_position)
		
		# Define first block
		self.generate_next_block()

	def generate_next_block(self):
		current_block_types_group = copy.deepcopy(self.block_types_group)
		for block_type in self.last_blocks_types:
			current_block_types_group.remove(block_type)

		random_block_type = current_block_types_group[random.randrange(len(current_block_types_group))]
		self.last_blocks_types.append(random_block_type)
		if len(self.last_blocks_types) > Constants.MAX_BLOCKS_OF_SAME_TYPE:
			del self.last_blocks_types[0]

		random_position = random.randrange(Constants.MAP_WIDTH - Constants.BLOCK_WIDTH)
		self.current_block = self.next_block
		self.next_block = Block(block_type=random_block_type, position=random_position)

	def get_current_block(self):
		return self.current_block

	def get_next_block(self):
		return self.next_block

	def print_next_block(self):
		assert self.next_block != None
		self.next_block.print_block()