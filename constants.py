import configparser
import shutil
import os

class Constants(object):
	AVAILABLE_UI_TYPES = ["CONSOLE", "GRAPHICAL"]
	
	DISPLAY_WIDTH = 600
	DISPLAY_HEIGHT = 450
	DISPLAY_SCALE = 1.0

	MAP_WIDTH = 12
	MAP_HEIGHT = 20

	MUSIC_SPEED_ACCELERATION_BY_LEVEL = 0.015

	LINE_ANIMATION_DURATION = 0.11

	@staticmethod
	def load_size_constants():
		Constants.WINDOW_WIDTH = int(20 * Constants.DISPLAY_SCALE)
		Constants.WINDOW_HEIGHT = int(30 * Constants.DISPLAY_SCALE)

		Constants.BLOCK_SIZE = int(20 * Constants.DISPLAY_SCALE)
		Constants.BLOCK_BORDER = 2.0 * Constants.DISPLAY_SCALE

		Constants.TOP_MARGIN_SPACE = int(15 * Constants.DISPLAY_SCALE)
		Constants.LEFT_MARGIN_SPACE = int(60 * Constants.DISPLAY_SCALE)
		Constants.BORDER_SPACE = int(5 * Constants.DISPLAY_SCALE)

		Constants.SCORE_HEIGHT = int(40 * Constants.DISPLAY_SCALE)

	FPS = 20.0
	TIME_BY_FRAME = 1.0 / FPS

	BLOCK_VELOCITY = 0.8
	TIME_REDUCTION_BY_LEVEL = 0.03625
	MARATHON_MAX_LEVEL = 20
	# UPDATE COLORS BY LEVEL IF THE MAX LEVEL IS CHANGED
	COLOR_BY_LEVEL = [
		"LIGHT_BLUE",
		"LIGHT_BLUE",
		"LIGHT_BLUE",
		"LIGHT_BLUE",
		"LIGHT_BLUE",
		"LIGHT_BLUE",
		"LIGHT_GREEN",
		"LIGHT_GREEN",
		"LIGHT_GREEN", 
		"LIGHT_GREEN",
		"LIGHT_GREEN",
		"LIGHT_YELLOW",
		"LIGHT_YELLOW",
		"LIGHT_YELLOW",
		"LIGHT_YELLOW",
		"LIGHT_ORANGE",
		"LIGHT_ORANGE",
		"LIGHT_ORANGE",
		"LIGHT_ORANGE",
		"LIGHT_RED"
	]

	BLOCK_WIDTH = 4
	BLOCK_HEIGHT = 4

	SCORE_BY_LINE = 9
	EXTRA_SCORE_BY_LEVEL = 1
	LINES_BY_LEVEL = 10

	MAX_BLOCKS_OF_SAME_TYPE = 4

	COLORS = {
		"RED": (255, 0, 0),
		"LIGHT_RED": (128, 0, 0),
		"BLACK": (0, 0, 0),
		"GREEN": (0, 255, 0),
		"LIGHT_GREEN": (0, 128, 0),
		"BLUE": (0, 0, 255),
		"LIGHT_BLUE": (0, 0, 128),
		"YELLOW": (255, 255, 0),
		"LIGHT_YELLOW": (128, 128, 0),
		"ORANGE": (255, 128, 0),
		"LIGHT_ORANGE": (128, 64, 0),
		"WHITE": (255, 255, 255),
		"GREY": (64, 64, 64)
	}

	BLOCK_COLORS = {
		1: COLORS["RED"],
		2: COLORS["GREEN"],
		3: COLORS["BLUE"],
		4: COLORS["YELLOW"],
		5: COLORS["ORANGE"]
	}

	TIME_BETWEEN_INPUTS = 0.09

	SOUND = True

	PLAY_SOUND = lambda sound_name: None
	STOP_SOUNDS = lambda : None

	BLINK_DURATION = 0.3
	BLINK_TIME = 1.0

	TEXT_FONT = "tahoma"
	MAX_NAME_SIZE = 8

	@staticmethod
	def set_is_sound_active():
		if Constants.SOUND:
			if shutil.which("play") is not None:
				Constants.PLAY_SOUND = lambda sound_name: os.system("play " + sound_name + " &")
				Constants.STOP_SOUNDS = lambda : os.system("pkill play")
			elif shutil.which("aplay") is not None:
				Constants.PLAY_SOUND = lambda sound_name: os.system("aplay " + sound_name + " &")
				Constants.STOP_SOUNDS = lambda : os.system("pkill aplay")
			else:
				print("No sound player available!")
		else:
			Constants.PLAY_SOUND = lambda sound_name: None
			Constants.STOP_SOUNDS = lambda : None

	@staticmethod
	def load_configs():
		config = configparser.ConfigParser()
		config.read("settings.ini")
		for setting in config["GENERAL"]:
			if setting == "sound":
				Constants.SOUND = "True" in config["GENERAL"][setting]
			elif setting == "display_width":
				Constants.DISPLAY_WIDTH = int(config["GENERAL"][setting])
				Constants.DISPLAY_SCALE = float(Constants.DISPLAY_WIDTH) / 600.0
			elif setting == "display_height":
				Constants.DISPLAY_HEIGHT = int(config["GENERAL"][setting])
			elif setting == "fps":
				Constants.FPS = float(config["GENERAL"][setting])
				Constants.TIME_BY_FRAME = 1.0 / Constants.FPS

		Constants.set_is_sound_active()

Constants.load_configs()
Constants.load_size_constants()