from state import State
from constants import Constants
import pygame
from end_game_state import EndGameState
import menu_state

class CreditsState(State):
	def __init__(self, state_manager):
		super(CreditsState, self).__init__(state_manager)

		self.title_font = pygame.font.SysFont(Constants.TEXT_FONT, int(50 * Constants.DISPLAY_SCALE))
		self.credits_font = pygame.font.SysFont(Constants.TEXT_FONT, int(20 * Constants.DISPLAY_SCALE))

	def draw(self, window):
		window.fill(Constants.COLORS["BLACK"])
		
		# Draw Credits at top with big letters
		label = self.title_font.render("Credits" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.05, Constants.DISPLAY_HEIGHT / 8))

		# Draw credits information
		label = self.credits_font.render("Creator: Daniel Ribeiro Margarido" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 10, Constants.DISPLAY_HEIGHT / 8 * 3))

		label = self.credits_font.render("Tools: Pygame, curses, configparser, play and aplay" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 10, Constants.DISPLAY_HEIGHT / 8 * 4))

		label = self.credits_font.render("Sounds: downloaded from 'eng.universal-soundbank.com'" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 10, Constants.DISPLAY_HEIGHT / 8 * 5))		

		label = self.credits_font.render("License: GPLv2" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 10, Constants.DISPLAY_HEIGHT / 8 * 6))

		pygame.display.update()

	def process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		if event.type == pygame.KEYDOWN:
			self.state_manager.set_state(menu_state.MenuState(self.state_manager))
			Constants.PLAY_SOUND("menu_move.wav")