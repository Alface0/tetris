import curses
import sys
from constants import *
from state import State
import pygame

class EndGameState(State):
	def __init__(self, state_manager):
		super(EndGameState, self).__init__(state_manager)

	def is_game_finished(self):
		return True

class ScoreGameState(State):
	def __init__(self, state_manager, data={}):
		super(ScoreGameState, self).__init__(state_manager)
		self.scores = []
		self.name = data["name"]

		# Load highscores from file
		scores_file = open("scores.txt", "r")
		for line in scores_file.readlines():
			line_data = line.strip().split(";")
			self.scores.append({
				"name": line_data[0],
				"level": int(line_data[1]),
				"score": int(line_data[2])
			})

		scores_file.close()

		# New score received
		if "name" in data and "level" in data and "score" in data:
			self.scores.append({
				"name": data["name"],
				"level": data["level"],
				"score": data["score"]	
			})

			self.scores = sorted(self.scores, key=lambda score: score["score"])[::-1]

			# Register top 5 scores
			scores_file = open("scores.txt", "w")
			for score in self.scores[:5]:
				scores_file.write(score["name"] + ";" + str(score["level"]) + ";" + str(score["score"]) + "\n")

			scores_file.close()

		self.title_font = pygame.font.SysFont(Constants.TEXT_FONT, int(30 * Constants.DISPLAY_SCALE))
		self.my_font = pygame.font.SysFont(Constants.TEXT_FONT, int(20 * Constants.DISPLAY_SCALE))
		self.info_font = pygame.font.SysFont(Constants.TEXT_FONT, int(16 * Constants.DISPLAY_SCALE))

		self.elapsed_time = 0.0
		self.instructions_visible = True

	def draw(self, window):
		window.fill(Constants.COLORS["BLACK"])

		# Draw Highscore titles
		label = self.title_font.render("Name" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 6, Constants.DISPLAY_HEIGHT / 8))

		label = self.title_font.render("Level" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 6 * 2.5, Constants.DISPLAY_HEIGHT / 8))

		label = self.title_font.render("Score" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 6 * 4, Constants.DISPLAY_HEIGHT / 8))

		# Draw Scores
		i = 0
		for score in self.scores:
			label = self.my_font.render(score["name"] ,1, Constants.COLORS["WHITE"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 6, Constants.DISPLAY_HEIGHT / 4 + i * Constants.SCORE_HEIGHT))

			label = self.my_font.render(str(score["level"]) ,1, Constants.COLORS["WHITE"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 6 * 2.5, Constants.DISPLAY_HEIGHT / 4 + i * Constants.SCORE_HEIGHT))

			label = self.my_font.render(str(score["score"]) ,1, Constants.COLORS["WHITE"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 6 * 4, Constants.DISPLAY_HEIGHT / 4 + i * Constants.SCORE_HEIGHT))

			i += 1

		# Draw Instructions
		if self.instructions_visible:
			label = self.info_font.render("Press 'r' to restart the game or 'q' to quit" ,1, Constants.COLORS["WHITE"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 5 + 15, Constants.DISPLAY_HEIGHT / 5 * 4))

		pygame.display.update()	

	def process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		if event.type == pygame.KEYDOWN:
			if event.key == ord("q"):
				self.state_manager.set_state(EndGameState(self.state_manager))

			if event.key == ord("r"):
				from in_game_state import InGameState
				self.state_manager.set_state(InGameState(
					state_manager=self.state_manager,
					data={"name": self.name})
				)

	def update(self, deltatime):
		self.elapsed_time += deltatime

		if self.instructions_visible:
			if self.elapsed_time >= Constants.BLINK_TIME:
				self.elapsed_time = 0.0
				self.instructions_visible = False
		else:
			if self.elapsed_time >= Constants.BLINK_DURATION:
				self.elapsed_time = 0.0
				self.instructions_visible = True
