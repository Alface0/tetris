import unittest
from unittest.mock import patch
from unittest import TestCase
from state_manager import StateManager
from in_game_state import InGameState
from end_game_state import EndGameState

class GameStateTransitionTest(TestCase):
	def setUp(self):
		pass

	@patch("in_game_state.InGameState.__init__", new=lambda self, ui_type:None)
	def test_state_change(self):
		manager = StateManager("GRAPHICAL")

		# InGameState is the inicial State
		self.assertTrue(isinstance(manager.current_state, InGameState))
		
		# Change the state to the EndGameState
		manager.set_state(EndGameState(manager))
		self.assertTrue(isinstance(manager.current_state, EndGameState))

if __name__ == "__main__":
	unittest.main()