from state import State
from block_builder import BlockBuilder
from constants import *
import random
import copy
import os

try:
	assert(ScoreGameState)
except:
	from end_game_state import EndGameState, ScoreGameState

import curses
import pygame

class InGameState(State):
	def __init__(self, state_manager, data={}):
		super(InGameState, self).__init__(state_manager)
		self.block_builder = BlockBuilder()
		self.game_map = [[0] * Constants.MAP_WIDTH for i in range(Constants.MAP_HEIGHT)]
		self.block = None
		self.time_elapsed = 0
		self.score = 0
		self.level = 1
		self.is_game_paused = False
		self.level_lines_count = 0

		self.state = "PLAYING"
		self.lines_to_remove = []
		self.animation_elapsed_time = 0.0
		self.line_animation_color = Constants.COLORS["BLACK"]

		self.block = self.block_builder.get_current_block()

		if self.state_manager.ui_type == "CONSOLE":
			self.__draw = lambda window, drawable_map: self.console_draw(window=window, drawable_map=drawable_map)
			self.__process_input = lambda event: self.console_process_input(event=event)

		elif self.state_manager.ui_type == "GRAPHICAL":
			self.__draw = lambda window, drawable_map: self.graphical_draw(window=window, drawable_map=drawable_map)
			self.__process_input = lambda event: self.graphical_process_input(event=event)
			self.my_font = pygame.font.SysFont(Constants.TEXT_FONT, int(21 * Constants.DISPLAY_SCALE))
			
			self.input_time_elapsed = 0.0
			self.holded_keys = set()

		self.song_time_elapsed = 0.0
		Constants.PLAY_SOUND("Tetris_by_Gori_Fater.wav speed +1")
		self.song_duration = 33 # Song duration at level 1

		if "name" in data:
			self.name = data["name"]
		else:
			self.name = "default"

	def draw(self, window):
		drawable_map = copy.deepcopy(self.game_map)

		# Set the block positions to the temporary map view
		for row, col in self.block.block_positions:
			drawable_map[row + self.block.y][col + self.block.x] = self.block.block[row][col]

		self.__draw(window=window, drawable_map=drawable_map)
	


	def console_draw(self, window, drawable_map):
		window.clear()

		# print map lines
		for row in range(Constants.MAP_HEIGHT):
			for col in range(Constants.MAP_WIDTH):
				if drawable_map[row][col] != 0:
					window.addstr(row, col, str(drawable_map[row][col]))

		window.refresh()

	def draw_block_borders(self, window, block):
		for row in range(Constants.BLOCK_HEIGHT):
			for col in range(Constants.BLOCK_WIDTH):
				if block.block[row][col] != 0:
					# Draw top border
					pygame.draw.rect(window, Constants.BLOCK_COLORS[block.block[row][col]], [
							int((block.x + col) * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							int((block.y + row) * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER),
							Constants.BLOCK_SIZE / 9
						]
					)

					# Draw bottom border
					pygame.draw.rect(window, Constants.BLOCK_COLORS[block.block[row][col]], [
							int((block.x + col) * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							int((block.y + row) * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER + Constants.BLOCK_SIZE - Constants.BLOCK_SIZE / 6),
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER),
							Constants.BLOCK_SIZE / 9
						]
					)

					# Draw left border
					pygame.draw.rect(window, Constants.BLOCK_COLORS[block.block[row][col]], [
							int((block.x + col) * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							int((block.y + row) * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							Constants.BLOCK_SIZE / 9,
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER)
						]
					)

					# Draw right border
					pygame.draw.rect(window, Constants.BLOCK_COLORS[block.block[row][col]], [
							int((block.x + col) * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER + Constants.BLOCK_SIZE - Constants.BLOCK_SIZE / 6),
							int((block.y + row) * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
							Constants.BLOCK_SIZE / 9,
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER)
						]
					)


	def graphical_draw(self, window, drawable_map):
		window.fill(Constants.COLORS["WHITE"])

		# Draw Transparent backgroud
		surface = pygame.Surface((Constants.DISPLAY_WIDTH, Constants.DISPLAY_HEIGHT))		
		surface.set_alpha(100)
		surface.fill(Constants.COLORS[Constants.COLOR_BY_LEVEL[self.level - 1]])
		window.blit(surface, (0, 0))

		menu_center_x = Constants.DISPLAY_WIDTH - (Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE * 2 + Constants.BLOCK_SIZE * Constants.MAP_WIDTH) / 2

		# Draw Border
		border_max_x = Constants.MAP_WIDTH * Constants.BLOCK_SIZE + Constants.BORDER_SPACE + Constants.LEFT_MARGIN_SPACE
		border_max_y = Constants.MAP_HEIGHT * Constants.BLOCK_SIZE + Constants.BORDER_SPACE + Constants.TOP_MARGIN_SPACE
		pygame.draw.rect(window, Constants.COLORS["BLACK"], [
			Constants.LEFT_MARGIN_SPACE, 
			Constants.TOP_MARGIN_SPACE, 
			border_max_x - Constants.LEFT_MARGIN_SPACE,
			border_max_y - Constants.TOP_MARGIN_SPACE])

		for x in range(Constants.LEFT_MARGIN_SPACE, border_max_x, Constants.BORDER_SPACE):
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [x, Constants.TOP_MARGIN_SPACE, Constants.BORDER_SPACE, Constants.BORDER_SPACE])
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [x, border_max_y, Constants.BORDER_SPACE, Constants.BORDER_SPACE])

		for x in range(Constants.TOP_MARGIN_SPACE, border_max_y, Constants.BORDER_SPACE):
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [Constants.LEFT_MARGIN_SPACE, x, Constants.BORDER_SPACE, Constants.BORDER_SPACE])
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [border_max_x, x, Constants.BORDER_SPACE, Constants.BORDER_SPACE])

		# Draw next block border
		block_space = Constants.BLOCK_WIDTH * Constants.BLOCK_SIZE 
		next_border_max_x = menu_center_x + block_space / 2 + Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 2
		next_border_max_y = (Constants.DISPLAY_HEIGHT / 6) * 3 + block_space + Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 0.5
		pygame.draw.rect(window, Constants.COLORS["BLACK"], [
			int((menu_center_x - block_space / 2) - Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 1.5),
			int((Constants.DISPLAY_HEIGHT / 6) * 3),
			int(next_border_max_x) - int((menu_center_x - block_space / 2) - Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 1.5),
			int(next_border_max_y) - int((Constants.DISPLAY_HEIGHT / 6) * 3)
		])

		for x in range(int((menu_center_x - block_space / 2) - Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 1.5), int(next_border_max_x), Constants.BORDER_SPACE):
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [x, (Constants.DISPLAY_HEIGHT / 6) * 3, Constants.BORDER_SPACE, Constants.BORDER_SPACE])
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [x, next_border_max_y, Constants.BORDER_SPACE, Constants.BORDER_SPACE])

		for x in range(int((Constants.DISPLAY_HEIGHT / 6) * 3), int(next_border_max_y), Constants.BORDER_SPACE):
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [(menu_center_x - block_space / 2) - Constants.BORDER_SPACE + Constants.BLOCK_SIZE * 1.5, x, Constants.BORDER_SPACE, Constants.BORDER_SPACE])
		    pygame.draw.rect(window, Constants.COLORS["WHITE"], [next_border_max_x, x, Constants.BORDER_SPACE, Constants.BORDER_SPACE])


		# Draw Blocks
		for row in range(Constants.MAP_HEIGHT):
			for col in range(Constants.MAP_WIDTH):
				if drawable_map[row][col] != 0:
					if row in self.lines_to_remove:
						pygame.draw.rect(window, self.line_animation_color, [
								int(col * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
								int(row * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
								int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER),
								int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER)
							]
						)
					else:	
						pygame.draw.rect(window, Constants.BLOCK_COLORS[drawable_map[row][col]], [
								int(col * Constants.BLOCK_SIZE + Constants.LEFT_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
								int(row * Constants.BLOCK_SIZE + Constants.TOP_MARGIN_SPACE + Constants.BORDER_SPACE + Constants.BLOCK_BORDER),
								int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER),
								int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER)
							]
						)

		# Draw next block
		next_block = self.block_builder.get_next_block()
		for row in range(Constants.BLOCK_HEIGHT):
			for col in range(Constants.BLOCK_WIDTH):
				if next_block.block[row][col] != 0:
					pygame.draw.rect(window, Constants.BLOCK_COLORS[next_block.block[row][col]], [
							menu_center_x - block_space / 2 + Constants.BLOCK_SIZE * col + Constants.BLOCK_SIZE * 2,
							(Constants.DISPLAY_HEIGHT / 6) * 3 + Constants.BLOCK_SIZE * row + Constants.BLOCK_SIZE * 0.5,
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER),
							int(Constants.BLOCK_SIZE - Constants.BLOCK_BORDER)
						]
					)

		# Draw current block fall position
		current_block = self.block_builder.get_current_block()
		
		## Calculate fall position
		temporary_block = copy.deepcopy(current_block)
		is_block_at_bottom = False
		while not is_block_at_bottom:
			if self.block_collides_bottom(block=temporary_block):
				is_block_at_bottom = True
			else:
				temporary_block.y += 1

		## Draw Block Borders in the fall position
		self.draw_block_borders(window=window, block=temporary_block)

		# Draw Score
		label = self.my_font.render("Score: " + str(self.score), 1, Constants.COLORS["WHITE"])
		score_y = Constants.DISPLAY_HEIGHT / 6
		window.blit(label, (menu_center_x, score_y))

		# Draw Level
		label = self.my_font.render("Level: " + str(self.level), 1, Constants.COLORS["WHITE"])
		level_y = (Constants.DISPLAY_HEIGHT / 6) * 1.85
		window.blit(label, (menu_center_x, level_y))

		pygame.display.update()		

	def block_collides_left(self, block):
		for row, col in block.block_positions:
			# Pass to the next position if the current one is on left of other
			if (row, col - 1) in block.block_positions:
				continue
			
			# Collides if hit the left limit of the map
			if block.x + col <= 0:
				return True

			# Collides if hits another piece		
			if self.game_map[block.y + row][block.x + col - 1] != 0:
				return True

		return False

	def block_collides_right(self, block):
		for row, col in block.block_positions:
			# Pass to the next position if the current one is on left of other
			if (row, col + 1) in block.block_positions:
				continue
			
			# Collides if hit the right limit of the map
			if block.x + col >= Constants.MAP_WIDTH - 1:
				return True

			# Collides if hits another piece		
			if self.game_map[block.y + row][block.x + col + 1] != 0:
				return True

		return False

	def block_collides_bottom(self, block):
		for row, col in block.block_positions:
			# Pass to the next position if the current one is on top of other
			if (row + 1, col) in block.block_positions:
				continue

			# Collides if hit the bottom limit of the map
			if block.y + row >= Constants.MAP_HEIGHT - 1:
				return True

			# Collides if hit another piece		
			if self.game_map[block.y + row + 1][block.x + col] != 0:
				return True

		return False

	def clear_lines_to_remove(self):
		score_multiplier = 1

		for row in self.lines_to_remove:
			Constants.PLAY_SOUND("make_line.wav")

			self.score += ((Constants.SCORE_BY_LINE + Constants.EXTRA_SCORE_BY_LEVEL * self.level) * score_multiplier)

			# Trigger action to reset map above the line
			for above_row in range(row, 0, -1):
				self.game_map[above_row] = self.game_map[above_row - 1][:]

			for col in range(Constants.MAP_WIDTH):
				self.game_map[0][col] = 0

			score_multiplier += 1
			if self.level < Constants.MARATHON_MAX_LEVEL:
				self.level_lines_count += 1
				if self.level_lines_count >= Constants.LINES_BY_LEVEL:
					self.level += 1
					self.level_lines_count = 0

		self.lines_to_remove = []


	# Apply the block to the map
	def apply_block_to_map(self):
		for row, col in self.block.block_positions:
			self.game_map[self.block.y + row][self.block.x + col] = self.block.block[row][col]

		# Check for full lines
		for row in range(Constants.MAP_HEIGHT):
			if 0 not in self.game_map[row]:
				self.lines_to_remove.append(row)
				self.state = "IN_ANIMATION"


	# Returns 0 if the block moved and 1 if the block was attached to the map
	def move_block(self):	
		# Collision detection If it collides with other blocks or with the ground
		if not self.block_collides_bottom(block=self.block):
			self.block.y += 1
			return 0
		else:
			# Apply the block to the map as permanent fixed individual pieces
			self.apply_block_to_map()

			# Generate New Block
			self.block_builder.generate_next_block()
			self.block = self.block_builder.get_current_block()

			return 1

	def update(self, deltatime):
		self.song_time_elapsed += deltatime

		if self.song_time_elapsed >= self.song_duration:
			self.song_duration = 33 / (1 + Constants.MUSIC_SPEED_ACCELERATION_BY_LEVEL * (self.level - 1))
			Constants.PLAY_SOUND("Tetris_by_Gori_Fater.wav speed +" + str(1.0 + Constants.MUSIC_SPEED_ACCELERATION_BY_LEVEL * (self.level - 1)))
			self.song_time_elapsed = 0.0

		if not self.is_game_paused:
			self.time_elapsed += deltatime

			if self.state == "PLAYING":
				if self.time_elapsed >= Constants.BLOCK_VELOCITY - (Constants.TIME_REDUCTION_BY_LEVEL * self.level):
					# If block collides when at top the player loses
					if self.block.y == 0 and self.block_collides_bottom(block=self.block):
						self.state_manager.set_state(ScoreGameState(self.state_manager, {
							"name": self.name,
							"level": self.level,
							"score": self.score
						}))

					is_applied_to_map = self.move_block()
					self.time_elapsed = 0.0

				if self.state_manager.ui_type == "GRAPHICAL":
						self.input_time_elapsed += deltatime
						if self.input_time_elapsed > Constants.TIME_BETWEEN_INPUTS:
							for key in self.holded_keys:
								if key == pygame.K_LEFT:
									if not self.block_collides_left(self.block):
										self.block.x -= 1

								elif key == pygame.K_RIGHT:
									if not self.block_collides_right(self.block):
										self.block.x += 1

								elif key == pygame.K_DOWN:
									if self.block.y + Constants.BLOCK_HEIGHT < Constants.MAP_HEIGHT - 1:
										self.move_block()

								self.input_time_elapsed = 0.0
			elif self.state == "IN_ANIMATION":
				self.animation_elapsed_time += deltatime
				extra_color_by_frame = (255 - Constants.COLORS["BLACK"][0]) / (Constants.LINE_ANIMATION_DURATION / Constants.TIME_BY_FRAME)
				self.line_animation_color = (
					self.line_animation_color[0] + extra_color_by_frame,
					self.line_animation_color[1] + extra_color_by_frame,
					self.line_animation_color[2] + extra_color_by_frame
				)

				if self.animation_elapsed_time >= Constants.LINE_ANIMATION_DURATION:
					self.clear_lines_to_remove()
					self.state = "PLAYING"
					self.animation_elapsed_time = 0.0
					self.line_animation_color = Constants.COLORS["BLACK"]

	def rotate_block_in_map(self):
		block_before_rotation = copy.deepcopy(self.block)

		self.block.rotate_block()

		collided_left = False
		collided_right = False

		# Push it from out of the map to inside
		if self.block_collides_left(self.block):
			collided_left = True
			while self.block_collides_left(self.block):
				self.block.x += 1

			self.block.x -= 1

		if self.block_collides_right(self.block):
			collided_right = True
			while self.block_collides_right(self.block):
				self.block.x -= 1

			self.block.x += 1

		if collided_left and collided_right:
			self.block = block_before_rotation


	def process_input(self, event):
		self.__process_input(event)

	def graphical_process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		# Key Pressed
		if event.type == pygame.KEYDOWN:
			if event.key == ord("p"):
				self.is_game_paused = not self.is_game_paused

			if not self.is_game_paused:
				if event.key == pygame.K_LEFT:
					self.holded_keys.add(pygame.K_LEFT)

				if event.key == pygame.K_RIGHT:
					self.holded_keys.add(pygame.K_RIGHT)

				if event.key == pygame.K_DOWN:
					self.holded_keys.add(pygame.K_DOWN)

				if event.key == pygame.K_UP:
					self.rotate_block_in_map()

					if Constants.SOUND:
						Constants.PLAY_SOUND("movement.wav")

				# Space press
				if event.key == pygame.K_SPACE:
					is_block_at_bottom = False
					while not is_block_at_bottom:
						if self.move_block() == 1:
							is_block_at_bottom = True
					
					if Constants.SOUND:
						Constants.PLAY_SOUND("drop_block.wav")

		# Key Released
		if event.type == pygame.KEYUP:
			if event.key == pygame.K_LEFT:
				if pygame.K_LEFT in self.holded_keys:
					self.holded_keys.remove(pygame.K_LEFT)

			if event.key == pygame.K_RIGHT:
				if pygame.K_RIGHT in self.holded_keys:
					self.holded_keys.remove(pygame.K_RIGHT)

			if event.key == pygame.K_DOWN:
				if pygame.K_DOWN in self.holded_keys:
					self.holded_keys.remove(pygame.K_DOWN)

	def console_process_input(self, event):
		if event == ord("p"):
			self.is_game_paused = not self.is_game_paused

		if event == ord('q'):
			self.state_manager.set_state(EndGameState(self.state_manager))

		if not self.is_game_paused:
			if event == curses.KEY_LEFT:
				if not self.block_collides_left(self.block):
					self.block.x -= 1

			if event == curses.KEY_RIGHT:
				if not self.block_collides_right(self.block):
					self.block.x += 1

			if event == curses.KEY_DOWN:
				if self.block.y + BLOCK_HEIGHT < MAP_HEIGHT - 1:
					self.move_block()

			if event == curses.KEY_UP:
				self.rotate_block_in_map()

				if Constants.SOUND:
					Constants.PLAY_SOUND("movement.wav")

			# Space press
			if event == 32:
				is_block_at_bottom = False
				while not is_block_at_bottom:
					if self.move_block() == 1:
						is_block_at_bottom = True

				if Constants.SOUND:
					Constants.PLAY_SOUND("drop_block.wav")

	def dispose(self):
		if self.state_manager.ui_type == "GRAPHICAL":
			Constants.STOP_SOUNDS()