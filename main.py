from block_builder import BlockBuilder
import time
import os
from constants import *
import random
import copy
from state_manager import *
from end_game_state import EndGameState
import sys
import time

class Main(object):
	def __init__(self, ui_type):
		if ui_type == "CONSOLE":
			self.screen = curses.initscr()
			curses.noecho()
			curses.cbreak()
			screen.keypad(1)
			screen.nodelay(1)
			curses.curs_set(0)

			self.get_char = lambda: [screen.getch()]
			self.dispose = lambda: curses.nocbreak(); screen.keypad(0); curses.echo(); curses.endwin()

		elif ui_type == "GRAPHICAL":
			pygame.init()
			self.screen = pygame.display.set_mode((Constants.DISPLAY_WIDTH, Constants.DISPLAY_HEIGHT))
			pygame.display.set_caption("Tetris")

			self.get_char = lambda: pygame.event.get()
			self.dispose = lambda: pygame.quit()		

if __name__ == "__main__":
	if len(sys.argv) < 2:
		ui_type = "GRAPHICAL"
	else:
		ui_type = sys.argv[1]

	if ui_type not in Constants.AVAILABLE_UI_TYPES:
		print("The available ui types are", Constants.AVAILABLE_UI_TYPES)
		sys.exit(0)

	if ui_type == "CONSOLE":
		import curses
	elif ui_type == "GRAPHICAL":
		import pygame 

	main = Main(ui_type)

	state_manager = StateManager(ui_type=ui_type)
	last_frame_time = time.time()

	# Game Loop
	while not state_manager.is_game_finished():
		current_time = time.time()

		if current_time >= (last_frame_time + Constants.TIME_BY_FRAME):
			for event in main.get_char():
				if event:
					state_manager.process_input(event=event)

			state_manager.update(current_time - last_frame_time)
			state_manager.draw(main.screen)

			last_frame_time = current_time
		else:
			time.sleep(last_frame_time + Constants.TIME_BY_FRAME - current_time)

	main.dispose()