from state import State
import pygame
from constants import *
from end_game_state import EndGameState
from select_name_state import SelectNameState
from credits_state import CreditsState
from main import Main


class SettingsState(State):
	def __init__(self, state_manager, data={}):
		super(SettingsState, self).__init__(state_manager)
		self.data = data
		self.menu = ["Sound", "Resolution", "FPS", "Back"]
		self.settings_options = {
			"Sound": [
				("On", True),
				("Off", False)
			],
			"Resolution": [
				("600x450", (600, 450)),
				("800x600", (800, 600)),
				("1024x768", (1024, 768))
			],
			"FPS": [
				("20", 20.0),
				("30", 30.0),
				("45", 45.0),
				("60", 60.0)
			]
		}

		if Constants.SOUND:
			sound = "On"
		else:
			sound = "Off"

		self.current_settings = {
			"Sound": sound,
			"Resolution": str(Constants.DISPLAY_WIDTH) + "x" + str(Constants.DISPLAY_HEIGHT),
			"FPS": str(Constants.FPS).split(".")[0]
		}

		self.selected_option = 0
		self.reload_fonts()

	def reload_fonts(self):
		self.title_font = pygame.font.SysFont(Constants.TEXT_FONT, int(50 * Constants.DISPLAY_SCALE))
		self.options_font = pygame.font.SysFont(Constants.TEXT_FONT, int(20 * Constants.DISPLAY_SCALE))


	def draw(self, window):
		window.fill(Constants.COLORS["BLACK"])

		# Draw Settings at top with big letters
		label = self.title_font.render("Settings" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.05, Constants.DISPLAY_HEIGHT / 8))

		i = 0
		for menu_item in self.menu:
			if i == self.selected_option:
				label = self.options_font.render(menu_item ,1, Constants.COLORS["WHITE"])
			else:
				label = self.options_font.render(menu_item ,1, Constants.COLORS["GREY"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 8, Constants.DISPLAY_HEIGHT / 3 + i * Constants.SCORE_HEIGHT * 1.25))
			if menu_item in self.settings_options:

				# If its a setting, render the possible options
				current_option = 1
				for option in self.settings_options[menu_item]:
					if option[0] == self.current_settings[menu_item]:
						option_label = self.options_font.render(option[0] ,1, Constants.COLORS["WHITE"])
					else: 
						option_label = self.options_font.render(option[0] ,1, Constants.COLORS["GREY"])
					window.blit(option_label, (Constants.DISPLAY_WIDTH / 6 * (current_option + 1), Constants.DISPLAY_HEIGHT / 3 + i * Constants.SCORE_HEIGHT * 1.25))
					current_option += 1
			i += 1

		pygame.display.update()
	


	def set_setting_value(self, setting, value):
		if setting in self.current_settings:

			# Generate initial config file data
			config = configparser.ConfigParser()
			config["GENERAL"] = {
				"SOUND": Constants.SOUND,
				"DISPLAY_WIDTH": Constants.DISPLAY_WIDTH,
				"DISPLAY_HEIGHT": Constants.DISPLAY_HEIGHT,
				"FPS": Constants.FPS
			}

			self.current_settings[setting] = value[0]
			if setting == "Sound":
				Constants.SOUND = value[1]
				Constants.set_is_sound_active()
				config["GENERAL"]["SOUND"] = str(Constants.SOUND)

			if setting == "Resolution":
				Constants.DISPLAY_WIDTH = value[1][0]
				Constants.DISPLAY_HEIGHT = value[1][1]
				config["GENERAL"]["DISPLAY_WIDTH"] = str(Constants.DISPLAY_WIDTH)
				config["GENERAL"]["DISPLAY_HEIGHT"] = str(Constants.DISPLAY_HEIGHT)

				Main.screen = pygame.display.set_mode((Constants.DISPLAY_WIDTH, Constants.DISPLAY_HEIGHT))
				Constants.DISPLAY_SCALE = float(Constants.DISPLAY_WIDTH) / 600.0
				Constants.load_size_constants()
				self.reload_fonts()

			if setting == "FPS":
				Constants.FPS = value[1]
				Constants.TIME_BY_FRAME = 1.0 / Constants.FPS
				config["GENERAL"]["FPS"] = str(Constants.FPS)
			
			# Register settings changes
			with open("settings.ini", "w") as config_file:
				config.write(config_file)


	def process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_DOWN:
				if self.selected_option < len(self.menu) - 1:
					self.selected_option += 1
					Constants.PLAY_SOUND("menu_move.wav")

			if event.key == pygame.K_UP:
				if self.selected_option > 0:
					self.selected_option -= 1
					Constants.PLAY_SOUND("menu_move.wav")

			if event.key == pygame.K_RETURN:
				if self.menu[self.selected_option] == "Back":
					self.state_manager.set_state(MenuState(self.state_manager))
					Constants.PLAY_SOUND("menu_move.wav")

			if self.menu[self.selected_option] in self.settings_options:
				options_list = [option[0] for option in self.settings_options[self.menu[self.selected_option]]]
				list_selected_option = options_list.index(self.current_settings[self.menu[self.selected_option]])
				if event.key == pygame.K_RIGHT:
					if list_selected_option < len(options_list) - 1:
						list_selected_option += 1
						self.set_setting_value(
							setting=self.menu[self.selected_option],
						 	value=self.settings_options[self.menu[self.selected_option]][list_selected_option]
						)
						

				if event.key == pygame.K_LEFT:
					if list_selected_option > 0:
						list_selected_option -= 1
						self.set_setting_value(
							setting=self.menu[self.selected_option],
						 	value=self.settings_options[self.menu[self.selected_option]][list_selected_option]
						)
						
				Constants.PLAY_SOUND("menu_move.wav")

class MenuState(State):
	def __init__(self, state_manager, data={}):
		super(MenuState, self).__init__(state_manager)
		self.data = data
		self.menu = [
			"Play",
			"Settings",
			"Credits",
			"Quit"
		]

		self.selected_option = 0
		self.title_font = pygame.font.SysFont(Constants.TEXT_FONT, int(50 * Constants.DISPLAY_SCALE))
		self.options_font = pygame.font.SysFont(Constants.TEXT_FONT, int(20 * Constants.DISPLAY_SCALE))

	def draw(self, window):
		window.fill(Constants.COLORS["BLACK"])

		# Draw TETRIS at top with big letters
		label = self.title_font.render("TETRIS" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.05, Constants.DISPLAY_HEIGHT / 8))

		# Draw menu
		i = 0
		for option in self.menu:
			if i == self.selected_option:
				label = self.options_font.render(option ,1, Constants.COLORS["WHITE"])
			else:	
				label = self.options_font.render(option ,1, Constants.COLORS["GREY"])
			window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.2, Constants.DISPLAY_HEIGHT / 3 + i * Constants.SCORE_HEIGHT))
			i += 1

		pygame.display.update()
		

	def process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_DOWN:
				if self.selected_option < len(self.menu) - 1:
					self.selected_option += 1
					Constants.PLAY_SOUND("menu_move.wav")

			if event.key == pygame.K_UP:
				if self.selected_option > 0:
					self.selected_option -= 1
					Constants.PLAY_SOUND("menu_move.wav")

			if event.key == pygame.K_RETURN:
				Constants.PLAY_SOUND("menu_move.wav")
				if self.menu[self.selected_option] == "Quit":
					self.state_manager.set_state(EndGameState(self.state_manager))

				elif self.menu[self.selected_option] == "Play":
					self.state_manager.set_state(SelectNameState(self.state_manager))

				elif self.menu[self.selected_option] == "Settings":
					self.state_manager.set_state(SettingsState(self.state_manager))

				elif self.menu[self.selected_option] == "Credits":
					self.state_manager.set_state(CreditsState(self.state_manager))
