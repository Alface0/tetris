from state import State
import pygame
from constants import *
from in_game_state import InGameState
from end_game_state import EndGameState

VALID_NAME_CHARS = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", 
"r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L",
"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "-", "0", "1", "2", "3", "4", "5",
"6", "7", "8", "9", ".", " "]

VALID_CHARS = {}
for char_name in VALID_NAME_CHARS:
	VALID_CHARS[ord(char_name)] = char_name


class SelectNameState(State):
	def __init__(self, state_manager, data={}):
		self.state_manager = state_manager
		self.title_font = pygame.font.SysFont(Constants.TEXT_FONT, int(25 * Constants.DISPLAY_SCALE))
		self.input_font = pygame.font.SysFont(Constants.TEXT_FONT, int(20 * Constants.DISPLAY_SCALE))
		self.cursor_font = pygame.font.SysFont(Constants.TEXT_FONT, int(14 * Constants.DISPLAY_SCALE))

		self.text_stack = []

		self.elapsed_time = 0.0
		self.is_cursor_visible = True
		self.cursor_blinking_time = 0.2
		self.delay_between_cursor_blinks = 0.5

	def draw(self, window):
		window.fill(Constants.COLORS["BLACK"])

		# Draw title label
		label = self.title_font.render("INSERT YOUR NAME" ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 4 * 1.2, Constants.DISPLAY_HEIGHT / 8))

		# Draw input field
		label = self.input_font.render("".join(self.text_stack) ,1, Constants.COLORS["WHITE"])
		window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.28, Constants.DISPLAY_HEIGHT / 4))

		# Draw blinking cursor
		if self.is_cursor_visible:
			if len(self.text_stack) < Constants.MAX_NAME_SIZE:
				label = self.cursor_font.render("".join("_") ,1, Constants.COLORS["WHITE"])
				window.blit(label, (Constants.DISPLAY_WIDTH / 3 * 1.28 + (10 * Constants.DISPLAY_SCALE * len(self.text_stack)), Constants.DISPLAY_HEIGHT / 4 + (10 * Constants.DISPLAY_SCALE)))			

		pygame.display.update()

	def update(self, deltatime):
		self.elapsed_time += deltatime
		
		if self.is_cursor_visible:
			if self.elapsed_time >= self.delay_between_cursor_blinks:
				self.elapsed_time = 0
				self.is_cursor_visible = False
		else:
			if self.elapsed_time >= self.cursor_blinking_time:
				self.elapsed_time = 0
				self.is_cursor_visible = True

	def process_input(self, event):
		if event.type == pygame.QUIT:
			self.state_manager.set_state(EndGameState(self.state_manager))

		if event.type == pygame.KEYDOWN:
			if event.key in VALID_CHARS:
				if len(self.text_stack) < Constants.MAX_NAME_SIZE:
					self.text_stack.append(VALID_CHARS[event.key])

			if event.key == pygame.K_RETURN:
				Constants.PLAY_SOUND("menu_move.wav")
				self.state_manager.set_state(InGameState(
					state_manager=self.state_manager,
					data={"name": "".join(self.text_stack)})
				)

			if event.key == pygame.K_BACKSPACE:
				Constants.PLAY_SOUND("menu_move.wav")
				if len(self.text_stack) > 0:
					self.text_stack.pop()
