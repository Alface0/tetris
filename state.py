from constants import *

class State(object):
	def __init__(self, state_manager):
		self.state_manager = state_manager

	def draw(self, window):
		pass

	def update(self, deltatime):
		pass

	def process_input(self, event):
		pass

	def dispose(self):
		pass

	def is_game_finished(self):
		return False
