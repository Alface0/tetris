from in_game_state import InGameState
from menu_state import MenuState

class StateManager(object):
	def __init__(self, ui_type):
		# Initial State
		self.ui_type = ui_type
		if ui_type == "GRAPHICAL":
			self.current_state = MenuState(self)
		else:
			self.current_state = InGameState(self)

	def draw(self, window):
		self.current_state.draw(window)

	def update(self, deltatime):
		self.current_state.update(deltatime)

	def process_input(self, event):
		self.current_state.process_input(event)

	def set_state(self, state):
		self.current_state.dispose()
		self.current_state = state

	def is_game_finished(self):
		return self.current_state.is_game_finished()